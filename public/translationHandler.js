document.body.onload = () => {
	console.log('Body loaded');
	if (window.location.hostname === 'es-bikespot-demo.onelink-translations.com') {
		const script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://www.onelink-edge.com/xapis/Pretranslate/P15DE-DD77-3D85-F90B.js';
		document.querySelector('head').appendChild(script);
	} else {
		const param = getParameterByName('lang', window.location.href);
		if (param !== null) {
			const script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = "https://www.onelink-edge.com/xapis/Pretranslate/9A89-8D75-5124-5D30.js?lang='pa-pk'";
			document.querySelector('head').appendChild(script);
		}
	}
};

function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, '\\$&');
	var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
