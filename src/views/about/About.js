import React from 'react';

// reactstrap components
import { Row, Col, Card, CardImg, CardBody, CardTitle, CardText, Button, Container } from 'reactstrap';

// core components
import IndexNavbar from 'components/Navbars/IndexNavbar.js';
import AboutUsHeader from './AboutHeader';
import DarkFooter from 'components/Footers/DarkFooter.js';
import Contact from 'components/Contact/Contact';

// sections for this page

import 'index.css';

function About() {
	React.useEffect(() => {
		document.body.classList.add('index-page');
		document.body.classList.add('sidebar-collapse');
		document.documentElement.classList.remove('nav-open');
		window.scrollTo(0, 0);
		document.body.scrollTop = 0;
		return function cleanup() {
			document.body.classList.remove('index-page');
			document.body.classList.remove('sidebar-collapse');
		};
	});
	const team = [
		{
			name: 'Theia Sawyer',
			position: 'CTO',
			picture:
				'https://images.pexels.com/photos/3727474/pexels-photo-3727474.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=400&w=400'
		},
		{
			name: 'Eliott Leblanc',
			position: 'CEO',
			picture:
				'https://images.pexels.com/photos/2182970/pexels-photo-2182970.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
		},
		{
			name: 'Judith Marks',
			position: 'CFO',
			picture:
				'https://images.pexels.com/photos/1024311/pexels-photo-1024311.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
		},
		{
			name: 'Vlad Martins',
			position: 'CMO',
			picture:
				'https://images.pexels.com/photos/3460134/pexels-photo-3460134.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
		}
	];
	return (
		<div>
			<IndexNavbar />
			<div className="wrapper">
				<AboutUsHeader />
				<div className="main">
					<br />
					<section className="about-section">
						<Row>
							<Col lg="4" data-aos="zoom-in">
								<h2>WHO WE ARE</h2>
								<p>
									We started small and grew our e-commerce business over the time. With the trust of
									our customers, and our amazing service, we now have over 100k orders a day! <br />
								</p>
								<br />
								<br />
								<br />
								<br />
								<hr />
								<div style={{ textAlign: 'right' }}>
									<h2>WHAT WE DO</h2>
									<p>
										We started small and grew our e-commerce business over the time. With the trust
										of our customers, and our amazing service, we now have over 100k orders a day!{' '}
										<br />
									</p>
								</div>
								<br />
							</Col>
							<Col lg="8" data-aos="fade-left">
								<img
									src={require('assets/img/dedicated-team-text.jpg')}
									alt="team"
									className="section-image"
								/>
							</Col>
						</Row>
					</section>
					<hr />
					<section className="about-section">
						<Container>
							<h2 style={{ textAlign: 'center' }}>OUR TEAM</h2>
							<br />
							<div className="team">
								<Row>
									{team.map((member, index) => (
										<Col md="3" data-aos="zoom-in" key={index}>
											<div className="team-player">
												<img
													alt="..."
													className="rounded-circle img-fluid img-raised team-member-picture"
													src={member.picture}
												/>
												<h4 className="title OneLinkNoTx">{member.name}</h4>
												<p className="category text-info">{member.position}</p>
												<p className="description">
													You can write here details about one of your team members. You can
													give more details about what they do. Feel free to add some{' '}
													<a href="#pablo" onClick={(e) => e.preventDefault()}>
														links
													</a>{' '}
													for people to be able to follow them outside the site.
												</p>
												<Button
													className="btn-icon btn-round"
													color="info"
													href="#pablo"
													onClick={(e) => e.preventDefault()}
												>
													<i className="fab fa-twitter" />
												</Button>
												<Button
													className="btn-icon btn-round"
													color="info"
													href="#pablo"
													onClick={(e) => e.preventDefault()}
												>
													<i className="fab fa-instagram" />
												</Button>
												<Button
													className="btn-icon btn-round"
													color="info"
													href="#pablo"
													onClick={(e) => e.preventDefault()}
												>
													<i className="fab fa-facebook-square" />
												</Button>
											</div>
										</Col>
									))}
								</Row>
							</div>
						</Container>
					</section>
				</div>
				<hr />
				<DarkFooter />
			</div>
		</div>
	);
}

export default About;
