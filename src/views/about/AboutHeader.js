import React from 'react';

// reactstrap components
import { Button, Container } from 'reactstrap';

// core components

function AboutUsHeader() {
	let pageHeader = React.createRef();

	React.useEffect(() => {
		if (window.innerWidth > 991) {
			const updateScroll = () => {
				let windowScrollTop = window.pageYOffset / 3;
				pageHeader.current.style.transform = 'translate3d(0,' + windowScrollTop + 'px,0)';
			};
			window.addEventListener('scroll', updateScroll);
			return function cleanup() {
				window.removeEventListener('scroll', updateScroll);
			};
		}
	});
	return (
		<div data-aos="fade-in-leftR">
			<div className="page-header page-header-small">
				<div
					className="page-header-image"
					style={{
						backgroundImage:
							'url(https://images.pexels.com/photos/7070/space-desk-workspace-coworking.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260)'
					}}
					ref={pageHeader}
				/>
				<div className="content-left">
					<Container style={{ textAlign: 'left' }}>
						<h1 className="title">ABOUT US</h1>
						<p>
							We're a team of dedicated people that strives to provide <br /> an amazing service for
							people around the world
						</p>
					</Container>
				</div>
			</div>
		</div>
	);
}

export default AboutUsHeader;
