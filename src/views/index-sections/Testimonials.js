import React, { Component } from 'react';

// reactstrap components
import { Card, CardHeader, CardBody, CardTitle, CardText, CardFooter, Button } from 'reactstrap';

import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';
import UniqID from 'uniqid';
import testimonials from '../../data/testimonials.json';

// core components

class Testimonials extends Component {
	state = {
		testimonials: testimonials
	};

	render() {
		return (
			<div>
				<AwesomeSlider>
					{this.state &&
						this.state.testimonials &&
						this.state.testimonials.map((testimonial) => (
							<div className="team-player">
								<img
									alt="..."
									className="rounded-circle img-fluid img-raised team-member-picture"
									src={testimonial.image}
								/>
								<h4 className="title OneLinkNoTx">{testimonial.name}</h4>
								<p className="category testimonial-comment">
									{testimonial.comment} - <em>{testimonial.date}</em>
								</p>
								<div className="star-rating">
									{testimonial.rating.map((t) => (
										<i key={UniqID()} className="fas fa-star testimonial-rating" />
									))}
								</div>
							</div>
						))}
				</AwesomeSlider>
			</div>
		);
	}
}

export default Testimonials;
