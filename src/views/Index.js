import React, { useState, useEffect } from 'react';

// reactstrap components
import { Row, Col, Container } from 'reactstrap';

// core components
import IndexNavbar from 'components/Navbars/IndexNavbar.js';
import IndexHeader from 'components/Headers/IndexHeader.js';
import DarkFooter from 'components/Footers/DarkFooter.js';

// sections for this page
import Products from 'components/Products/Products';

import Testimonials from 'views/index-sections/Testimonials';
import Contact from 'components/Contact/Contact';

import 'index.css';

function Index() {
	React.useEffect(() => {
		document.body.classList.add('index-page');
		document.body.classList.add('sidebar-collapse');
		document.documentElement.classList.remove('nav-open');
		window.scrollTo(0, 0);
		document.body.scrollTop = 0;
		return function cleanup() {
			document.body.classList.remove('index-page');
			document.body.classList.remove('sidebar-collapse');
		};
	});

	useEffect(() => {
		console.log('Component mounted!');
	}, []);

	return (
		<div>
			<IndexNavbar />
			<div className="wrapper">
				<IndexHeader />
				<div className="main">
					<br />
					<br />
					<br />
					<h2 style={{ textAlign: 'center' }}>WHAT WE OFFER</h2>
					<div className="container-fluid">
						<Row>
							<Col lg="4" data-aos="fade-right">
								<Row>
									<Col lg="2">
										<i className="far fa-check-circle feature-icon" />
									</Col>
									<Col lg="6">
										<h3>Fast Delivery</h3>
										<p>
											{' '}
											We always deliver the products that you've ordered as fast as possible.
											Usually, it takes between 2-4 business days to have your order delivered.
										</p>
									</Col>
								</Row>
							</Col>
							<Col lg="4" data-aos="fade-right">
								<Row>
									<Col lg="2">
										<i className="far fa-check-circle feature-icon" />
									</Col>
									<Col lg="6">
										<h3>Verified Sellers</h3>
										<p>
											{' '}
											All of the products that we feature are from verified sellers. What you see
											is what you get. No need to worry about the quality of the product!
										</p>
									</Col>
								</Row>
							</Col>
							<Col lg="4" data-aos="fade-right">
								<Row>
									<Col lg="2">
										<i className="far fa-check-circle feature-icon" />
									</Col>
									<Col lg="6">
										<h3>24/7 Support</h3>
										<p>
											{' '}
											Our support team is always online and will help you with any issues or
											answer any questions that you have. Feel free to contact us!
										</p>
									</Col>
								</Row>
							</Col>
						</Row>
						<hr />
						<Products />
						<hr />
						<h2 style={{ textAlign: 'center' }} data-aos="flip-right">
							TESTIMONIALS
						</h2>
						<Testimonials />
						<hr />

						<Container style={{ padding: '30px' }}>
							<Row>
								<Col lg="6">
									<iframe
										src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2869.4864689999195!2d20.911365315785623!3d44.011340837215286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47572121a0c30043%3A0x3594ccd2adc08492!2sKralja%20Aleksandra%20I%20Kara%C4%91or%C4%91evic%CC%81a%2023%2C%20Kragujevac%2034000!5e0!3m2!1sen!2srs!4v1592830659702!5m2!1sen!2srs"
										width="500"
										height="450"
										frameborder="0"
										style={{ border: '0' }}
										aria-hidden="false"
										title="google maps"
									/>
								</Col>
								<Col lg="6">
									<Contact />
								</Col>
							</Row>
						</Container>
					</div>
				</div>
				<DarkFooter />
			</div>
		</div>
	);
}

export default Index;
