import React, { Component } from 'react';
// reactstrap components
import { Row, Col, Button, Card, CardBody, Badge, CardText, CardImg, CardTitle, Container } from 'reactstrap';

import products from '../../data/products.json';
import DarkFooter from 'components/Footers/DarkFooter';
import Navbar from 'components/Navbars/IndexNavbar';
import ImageGallery from 'react-image-gallery';
import ProductCard from 'components/Products/ProductCard';
import Rating from 'react-rating';
import UniqID from 'uniqid';

import CheckoutForm from 'components/Forms/CheckoutForm';
import { SpinnerComponent } from 'react-element-spinner';

// core components

class Cart extends Component {
	state = {
		products: products,
		noProducts: false,
		cartItems: [],
		loading: false
	};
	componentDidMount() {
		const Storage = JSON.parse(localStorage.getItem('shopping-cart'));
		if (Storage === null) {
			this.setState({ noProducts: true });
		} else {
			const cartItems = [];
			Storage.forEach((item, index) => {
				const match = this.state.products.find((product) => product.id === item);
				cartItems.push(match);
				if (index === Storage.length - 1) {
					this.setState({ cartItems: cartItems });
				}
			});
		}
	}

	CalculateTotal = () => {
		let price = 0;
		this.state.cartItems.forEach((item) => {
			price += item.price;
		});
		return price;
	};

	RemoveItem = (id) => {
		const filtered = this.state.cartItems.filter((item) => item.id !== id);
		this.setState({ cartItems: filtered });
		const ids = filtered.map((item) => item.id);
		localStorage.setItem('shopping-cart', JSON.stringify(ids));
	};

	SwitchView = () => {
		document.getElementById('items-display').classList = 'slide-left';
		document.getElementById('items-display').style.display = 'none';
		document.getElementById('checkout-form-display').classList = 'form-slide-left';
	};

	GoBack = () => {
		document.getElementById('items-display').classList = 'slide-right';
		setTimeout(() => {
			document.getElementById('items-display').style.display = 'block';
		}, 300);
		document.getElementById('checkout-form-display').classList = 'form-slide-right';
	};

	handleSubmit = (e) => {
		e.preventDefault();
		this.setState({ loading: true });
		setTimeout(() => {
			this.setState({ loading: false });
			this.ClearPage();
		}, 2000);
	};

	ClearPage = () => {
		document.getElementById('checkout-form-display').classList = 'slide-left';
		document.getElementById('order-submited').classList = 'form-slide-left';
		setTimeout(() => {
			document.getElementById('checkout-form-display').style.display = 'none';
		}, 200);
		localStorage.removeItem('shopping-cart');
	};

	render() {
		return (
			<div>
				<Navbar />
				<div className="wrapper" style={{ marginTop: '100px', minHeight: '83vh' }}>
					<br />
					<div id="items-display">
						{this.state.cartItems.length === 0 ? (
							<Container>
								<h2>Your cart is empty</h2>
							</Container>
						) : (
							<Container>
								{this.state &&
									this.state.cartItems &&
									this.state.cartItems.map(
										(item) =>
											item !== undefined ? (
												<div className="cart-item-container" key={item.id}>
													<Row>
														<Col lg="3" style={{ textAlign: 'center' }}>
															<div className="cart-item-image-container">
																<img
																	src={item.picture[0]}
																	alt="cart item"
																	className="cart-item-image"
																/>
															</div>
														</Col>
														<Col lg="9">
															<div className="product">
																<a href={`/product/${item.id}`}>
																	<h2 className="OneLinkNoTx">{item.title}</h2>
																</a>
																<span className="product-price">${item.price}</span>
																<br />
																<span
																	className="remove-item"
																	onClick={() => this.RemoveItem(item.id)}
																>
																	remove
																</span>
															</div>
														</Col>
													</Row>
													<hr />
												</div>
											) : (
												''
											)
									)}
								<div style={{ textAlign: 'right' }}>
									<span className="total">Total: ${this.CalculateTotal()}</span>
									<Button color="info" size="lg" onClick={() => this.SwitchView()}>
										CHECKOUT
									</Button>
								</div>
							</Container>
						)}
					</div>
					<div id="checkout-form-display">
						<Container>
							<CheckoutForm GoBack={this.GoBack} handleSubmit={this.handleSubmit} />
						</Container>
					</div>
					<div id="order-submited">
						<Container style={{ textAlign: 'center' }}>
							<i class="far fa-check-circle order-complete-icon" />
							<h2>Thank you! Your order has been received!</h2>
							<p className="tracking-code">
								Your tracking code is: #{UniqID()}-{UniqID()}{' '}
							</p>
						</Container>
					</div>
				</div>
				<SpinnerComponent loading={this.state.loading} message="Submitting your order..." position="global" />
				<DarkFooter />
			</div>
		);
	}
}

export default Cart;
