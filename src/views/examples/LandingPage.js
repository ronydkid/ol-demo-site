import React from 'react';

// reactstrap components
import { Button, Input, InputGroupAddon, InputGroupText, InputGroup, Container, Row, Col } from 'reactstrap';

// core components
import IndexNavbar from 'components/Navbars/IndexNavbar.js';
import LandingPageHeader from 'components/Headers/LandingPageHeader.js';
import DarkFooter from 'components/Footers/DarkFooter.js';
function LandingPage() {
	const [ firstFocus, setFirstFocus ] = React.useState(false);
	const [ lastFocus, setLastFocus ] = React.useState(false);
	React.useEffect(() => {
		document.body.classList.add('landing-page');
		document.body.classList.add('sidebar-collapse');
		document.documentElement.classList.remove('nav-open');
		return function cleanup() {
			document.body.classList.remove('landing-page');
			document.body.classList.remove('sidebar-collapse');
		};
	});
	return (
		<div>
			<IndexNavbar />
			<div className="wrapper">
				<LandingPageHeader />
				<div className="section section-contact-us text-center">
					<Container>
						<h2 className="title OneLinkTx">Need to get in touch with us?</h2>
						<p className="description OneLinkTx">Send us a message and we'll respond ASAP!</p>
						<Row>
							<Col className="text-center ml-auto mr-auto" lg="6" md="8">
								<InputGroup className={'input-lg' + (firstFocus ? ' input-group-focus' : '')}>
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="now-ui-icons users_circle-08" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="First Name..."
										type="text"
										className="OneLinkNoTx"
										onFocus={() => setFirstFocus(true)}
										onBlur={() => setFirstFocus(false)}
										style={{ marginTop: '0' }}
									/>
								</InputGroup>
								<InputGroup className={'input-lg' + (lastFocus ? ' input-group-focus' : '')}>
									<InputGroupAddon addonType="prepend">
										<InputGroupText>
											<i className="now-ui-icons ui-1_email-85" />
										</InputGroupText>
									</InputGroupAddon>
									<Input
										placeholder="Email..."
										type="text"
										className="OneLinkNoTx"
										onFocus={() => setLastFocus(true)}
										onBlur={() => setLastFocus(false)}
										style={{ marginTop: '0' }}
									/>
								</InputGroup>
								<div className="textarea-container">
									<Input
										cols="80"
										name="name"
										placeholder="Type a message..."
										rows="4"
										type="textarea"
										className="OneLinkNoTx"
									/>
								</div>
								<div className="send-button">
									<Button
										block
										className="btn-round"
										color="info"
										href="#pablo"
										onClick={(e) => e.preventDefault()}
										size="lg"
									>
										Send Message
									</Button>
								</div>
							</Col>
						</Row>
					</Container>
				</div>
				<DarkFooter />
			</div>
		</div>
	);
}

export default LandingPage;
