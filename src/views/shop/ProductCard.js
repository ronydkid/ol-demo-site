import React, { Component } from 'react';

// reactstrap components
import { Row, Col, Button, Form, FormGroup, Input } from 'reactstrap';

import AwesomeSlider from 'react-awesome-slider';
import 'react-awesome-slider/dist/styles.css';

import testimonials from '../../data/testimonials.json';

import Rating from 'react-rating';

// core components

class ProductCard extends Component {
	renderDetails = (id) => {
		window.location.pathname = `/product/${id}`;
	};
	render() {
		return (
			<div>
				<div className="product-container">
					<div className="image-container">
						<img src={this.props.image} alt="product" />
					</div>
					<div className="product-details">
						<h3 className="product-name" onClick={() => this.renderDetails(this.props.id)}>
							{this.props.title}
						</h3>
						<Rating
							initialRating={this.props.rating}
							fullSymbol={<i className="fas fa-star product-star" />}
							emptySymbol={<i className="far fa-star product-star" />}
							readonly
						/>
						<p className="product-description">{this.props.description}</p>
						<Button color="success">
							<i className="fas fa-tags" style={{ marginRight: '5px' }} />
							${this.props.price}
						</Button>
						<Button color="info" href="#pablo" className="shop-button" onClick={(e) => e.preventDefault()}>
							<i className="fas fa-shopping-cart" />
							Add to cart
						</Button>
					</div>
				</div>
			</div>
		);
	}
}

export default ProductCard;
