import React from 'react';

// reactstrap components
import { Button, Container } from 'reactstrap';

// core components

function AboutUsHeader() {
	return (
		<div>
			<div className="page-header page-header-small">
				<div
					className="page-header-image shop-header-image"
					style={{
						backgroundImage: 'url(' + require('assets/img/sale-cover.jpg') + ')'
					}}
				/>
				<div className="content-left">
					<Container style={{ textAlign: 'left' }} />
				</div>
			</div>
		</div>
	);
}

export default AboutUsHeader;
