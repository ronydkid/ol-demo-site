import React, { Component } from 'react';

// reactstrap components
import { Row, Col, Container } from 'reactstrap';

import products from '../../data/products.json';
import SaleBanner from './ShopBanner';
import DarkFooter from 'components/Footers/DarkFooter';
import Navbar from 'components/Navbars/IndexNavbar';
//import ProductCard from './ProductCard';
import ProductCard from 'components/Products/ProductCard';
// core components

import './productcard.css';

class Shop extends Component {
	state = {
		products: []
	};

	ShuffleArray = (array) => {
		let counter = array.length;
		while (counter > 0) {
			let index = Math.floor(Math.random() * counter);
			counter--;
			let temp = array[counter];
			array[counter] = array[index];
			array[index] = temp;
		}
		return array;
	};

	componentDidMount() {
		this.setState({ products: this.ShuffleArray(products) });
	}
	render() {
		return (
			<div>
				<Navbar />
				<div className="wrapper">
					<SaleBanner />
					<br />
					<div className="container-fluid">
						<section className="shop-products">
							<Container fluid>
								<Row>
									{this.state &&
										this.state.products &&
										this.state.products.map((product, index) => (
											<Col lg="3" key={product.id}>
												<ProductCard
													id={product.id}
													description={product.description}
													image={product.picture[0]}
													price={product.price}
													shipping={product.shipping}
													title={product.title}
													category={product.category}
													rating={product.reviews.rating}
												/>
											</Col>
										))}
								</Row>
							</Container>
						</section>
					</div>
					<DarkFooter />
				</div>
			</div>
		);
	}
}

export default Shop;
