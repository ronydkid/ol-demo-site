import React, { Component } from 'react';

// reactstrap components
import { Row, Col, Button, Card, CardBody, Badge, CardText, CardImg, CardTitle, Container } from 'reactstrap';

import products from '../../data/products.json';
import DarkFooter from 'components/Footers/DarkFooter';
import Navbar from 'components/Navbars/IndexNavbar';
import ImageGallery from 'react-image-gallery';
import ProductCard from 'components/Products/ProductCard';
import Rating from 'react-rating';

// core components

import './product.css';
import 'react-image-gallery/styles/css/image-gallery.css';
class Product extends Component {
	state = {
		products: products
	};
	componentDidMount() {
		const productID = this.props.match.params.product_id;
		const data = this.state.products.find((product) => product.id === productID);
		const moreProducts = [];
		this.setState({ data: data });
		for (var i = 0; i < 4; i++) {
			const rand = this.state.products[Math.floor(Math.random() * this.state.products.length)];
			moreProducts.push(rand);
			if (i === 3) {
				this.setState({ related: moreProducts });
			}
		}
	}

	AddItemToCart = (item) => {
		const Storage = localStorage.getItem('shopping-cart');
		if (Storage === null) {
			const items = [];
			items.push(item);
			localStorage.setItem('shopping-cart', JSON.stringify(items));
		} else {
			const parsed = JSON.parse(Storage);
			if (!parsed.find((id) => id === item)) {
				parsed.push(item);
				localStorage.setItem('shopping-cart', JSON.stringify(parsed));
			}
		}
	};

	render() {
		return (
			<div>
				<Navbar />
				<div className="wrapper" style={{ marginTop: '100px' }}>
					<br />
					{this.state.data ? (
						<Container fluid>
							<Row>
								<Col lg="6">
									<ImageGallery
										items={this.state.data.picture.map((pic) => ({
											original: pic,
											thumbnail: pic
										}))}
									/>
								</Col>
								<Col lg="6">
									<div className="product-details-section">
										<h2 className="product-name OneLinkNoTx">{this.state.data.title}</h2>
										<Rating
											initialRating={this.state.data.reviews.rating}
											fullSymbol={<i className="fas fa-star product-star" />}
											emptySymbol={<i className="far fa-star product-star empty-star" />}
											readonly
										/>
										<span>
											<span className="reviews-amount">
												{this.state.data.reviews.number} reviews
											</span>
										</span>
										<hr />
										<Row>
											<Col lg="6">
												<span className="product-price">${this.state.data.price}</span>
												<Badge color="info" className="shipping-info">
													<i class="fas fa-dolly" />
													{this.state.data.shipping}
												</Badge>
											</Col>
											<Col lg="6">
												<span className="product-select">select model</span>
											</Col>
										</Row>
										<hr />
										<h3>Details</h3>
										<p className="detailed-product-description">{this.state.data.description}</p>
										<Button color="info" onClick={() => this.AddItemToCart(this.state.data.id)}>
											<i className="fas fa-shopping-cart" />
											Add To Cart
										</Button>
									</div>
								</Col>
							</Row>
							<hr />
							<br />
							<h2 style={{ textAlign: 'center' }}>SIMILAR PRODUCTS</h2>
							<br />
							<Container fluid>
								<Row>
									{this.state.related ? (
										this.state.related.map((product) => (
											<Col lg="3">
												<ProductCard
													id={product.id}
													description={product.description}
													image={product.picture}
													price={product.price}
													shipping={product.shipping}
													title={product.title}
													category={product.category}
													rating={product.reviews.rating}
												/>
											</Col>
										))
									) : (
										''
									)}
								</Row>
							</Container>
							<hr />
							{/* <Row>
								<Col lg="12">
									<div className="reviews-container">
										<Row>
											<Col lg="3">
												<div className="user-details">john doe</div>
											</Col>
											<Col lg="9">
												<div className="comment">lorem ipsumus</div>
											</Col>
										</Row>
									</div>
								</Col>
							</Row> */}
						</Container>
					) : (
						<div className="product-not-found">
							<h2>Oops! We're sorry. We couldn't find what you're looking for</h2>
						</div>
					)}
					<DarkFooter />
				</div>
			</div>
		);
	}
}

export default Product;
