/*eslint-disable*/
import React from 'react';

// reactstrap components
import { Container } from 'reactstrap';

// core components

function IndexHeader() {
	let pageHeader = React.createRef();

	React.useEffect(() => {
		if (window.innerWidth > 991) {
			const updateScroll = () => {
				let windowScrollTop = window.pageYOffset / 3;
				pageHeader.current.style.transform = 'translate3d(0,' + windowScrollTop + 'px,0)';
			};
			window.addEventListener('scroll', updateScroll);
			return function cleanup() {
				window.removeEventListener('scroll', updateScroll);
			};
		}
	});

	return (
		<div>
			<div className="page-header clear-filter" filter-color="blue">
				<div className="page-header-image" ref={pageHeader} />
				<Container>
					<div className="content-center brand">
						<img alt="..." className="n-logo" src={require('assets/img/now-logo.png')} />
						<h1 className="h1-seo">Bikespot Design.</h1>
						<h3>The best online shop.</h3>
						<em>with you since September 30 1999 </em>
					</div>
				</Container>
			</div>
		</div>
	);
}

export default IndexHeader;
