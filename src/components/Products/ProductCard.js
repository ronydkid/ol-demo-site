import React, { Component } from 'react';

// reactstrap components
import { Button, Card, CardImg, CardBody, CardTitle, CardText, Badge } from 'reactstrap';
import Rating from 'react-rating';

// core components

class ProductCard extends Component {
	AddItemToCart = (e, item) => {
		const Storage = localStorage.getItem('shopping-cart');
		if (Storage === null) {
			const items = [];
			items.push(item);
			localStorage.setItem('shopping-cart', JSON.stringify(items));
		} else {
			const parsed = JSON.parse(Storage);
			if (!parsed.find((id) => id === item)) {
				parsed.push(item);
				localStorage.setItem('shopping-cart', JSON.stringify(parsed));
			}
		}

		const btn = e.target;
		btn.className = 'btn btn-success';
		btn.innerHTML = `<i class="fas fa-check"></i> Added to cart`;

		setTimeout(() => {
			btn.className = 'btn btn-info';
			btn.innerHTML = `<i class="fas fa-shopping-cart"></i> Add to cart`;
		}, 1000);
		console.log(btn);
	};

	render() {
		return (
			<Card style={{ width: '20rem', marginRight: '20px' }}>
				<CardImg alt="..." src={this.props.image} data-src={this.props.image} top />
				<Badge
					color="success"
					className="product-price-badge"
					href="#pablo"
					onClick={(e) => e.preventDefault()}
				>
					${this.props.price}
				</Badge>
				<CardBody>
					<CardTitle tag="h4">
						<a href={`/product/${this.props.id}`} className="product-link OneLinkNoTx">
							{this.props.title}
						</a>
					</CardTitle>
					<Rating
						initialRating={this.props.rating}
						fullSymbol={<i className="fas fa-star product-star" />}
						emptySymbol={<i className="far fa-star product-star empty-star" />}
						readonly
					/>
					<CardText className="product-description">{this.props.description}</CardText>
					<Button color="info" id={this.props.id} onClick={(e) => this.AddItemToCart(e, this.props.id)}>
						<i className="fas fa-shopping-cart" />
						Add to cart
					</Button>
				</CardBody>
			</Card>
		);
	}
}

export default ProductCard;
