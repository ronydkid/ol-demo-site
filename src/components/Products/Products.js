import React, { Component } from 'react';

// reactstrap components
import { Button, Container, Card, CardImg, CardBody, CardTitle, CardText, Row, Col, Badge } from 'reactstrap';

import products from '../../data/products.json';
import ProductCard from 'components/Products/ProductCard';

// core components

class Products extends Component {
	state = {
		products: products
	};
	componentDidMount() {
		console.log(this.state.products);
	}
	render() {
		return (
			<div style={{ marginTop: '120px' }}>
				<div className="content-center">
					<Container fluid>
						<div className="grid">
							<div
								className="span-row-2 front-page-image"
								style={{
									backgroundImage: 'url(' + require('assets/img/laptops.jpg') + ')'
								}}
							>
								<a href="/shop">
									<i className="fas fa-arrow-circle-right" />
								</a>
							</div>
							<div
								className="front-page-image"
								style={{
									backgroundImage: 'url(' + require('assets/img/TABLETS.jpg') + ')'
								}}
							>
								<a href="/shop">
									<i className="fas fa-arrow-circle-right" />
								</a>
							</div>

							<div
								className="front-page-image"
								style={{
									backgroundImage: 'url(' + require('assets/img/phones.jpg') + ')'
								}}
							>
								<a href="/shop">
									<i className="fas fa-arrow-circle-right" />
								</a>
							</div>
						</div>
					</Container>
				</div>
			</div>
		);
	}
}

export default Products;
