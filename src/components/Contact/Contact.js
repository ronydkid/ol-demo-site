import React, { Component } from 'react';

// reactstrap components
import { Row, Col, Button, Form, FormGroup, Input } from 'reactstrap';

// core components

class Contact extends Component {
	render() {
		return (
			<div className="get-in-touch">
				<h2>GET IN TOUCH</h2>
				<p>
					If you have any questions, or experiencing any issues, you can contact our support team that will
					respond to you in a matter of minutes
				</p>
				<Row>
					<Col lg="12" style={{ textAlign: 'center' }}>
						<FormGroup>
							<Input
								id="exampleFormControlTextarea1"
								className="OneLinkNoTx"
								rows="8"
								type="textarea"
								placeholder="Message"
							/>
						</FormGroup>
					</Col>
					<Col lg="12" style={{ textAlign: 'center' }}>
						<Form className="form-inline">
							<FormGroup>
								<input
									className="form-control contact-form-input OneLinkNoTx"
									placeholder="Email Address"
									id="staticEmail2"
									type="text"
								/>
							</FormGroup>
							<FormGroup className="mx-sm-3">
								<Input
									id="inputPassword2"
									className="contact-form-input OneLinkNoTx"
									placeholder="Full name"
									type="text"
								/>
							</FormGroup>
							<Button
								color="primary"
								className="contact-form-submit"
								type="submit"
								style={{ marginTop: '22px' }}
							>
								Send
							</Button>
						</Form>
					</Col>
				</Row>
			</div>
		);
	}
}

export default Contact;
