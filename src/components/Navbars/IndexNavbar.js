import React from 'react';
import { Link } from 'react-router-dom';
// reactstrap components
import {
	Button,
	Collapse,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	UncontrolledDropdown,
	NavbarBrand,
	Navbar,
	NavItem,
	NavLink,
	Nav,
	Container,
	UncontrolledTooltip,
	Dropdown
} from 'reactstrap';

function IndexNavbar() {
	const [ collapseOpen, setCollapseOpen ] = React.useState(false);
	const [ dropdownOpen, setDropdownOpen ] = React.useState(false);
	const toggle = () => setDropdownOpen((prevState) => !prevState);

	React.useEffect(() => {
		const lang = localStorage.getItem('lang');
		if (lang && !window.location.search.includes('lang=pa-pk')) {
			const urlParams = new URLSearchParams(window.location.search);
			urlParams.set('lang', 'pa-pk');
			window.location.search = urlParams;
		}
	}, []);

	const handleLanguageChange = (lang) => {
		switch (lang) {
			case 'en':
				localStorage.removeItem('lang');
				window.location.hostname = 'bikespot-demo.onelink-translations.com';
				window.location.href.replace('lang=pa-pk', '');
				break;
			case 'es':
				localStorage.removeItem('lang');
				window.location.hostname = 'es-bikespot-demo.onelink-translations.com';
				window.location.href.replace('lang=pa-pk', '');
				break;
			case 'pa':
				localStorage.setItem('lang', 'pa-pk');
				window.location.hostname = 'bikespot-demo.onelink-translations.com';
		}
	};

	return (
		<div>
			{collapseOpen ? (
				<div
					id="bodyClick"
					onClick={() => {
						document.documentElement.classList.toggle('nav-open');
						setCollapseOpen(false);
					}}
				/>
			) : null}
			<Navbar className={'fixed-top '} expand="lg" color="info">
				<Container>
					<div className="navbar-translate">
						<NavbarBrand href="/" id="navbar-brand">
							Bikespot Design
						</NavbarBrand>

						<button
							className="navbar-toggler navbar-toggler"
							onClick={() => {
								document.documentElement.classList.toggle('nav-open');
								setCollapseOpen(!collapseOpen);
							}}
							aria-expanded={collapseOpen}
							type="button"
						>
							<span className="navbar-toggler-bar top-bar" />
							<span className="navbar-toggler-bar middle-bar" />
							<span className="navbar-toggler-bar bottom-bar" />
						</button>
					</div>
					<Collapse className="justify-content-end" isOpen={collapseOpen} navbar>
						<Nav navbar>
							<NavItem>
								<NavLink href="/index">
									<p>Home</p>
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="/about">
									<p>About</p>
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="/shop">
									<p>Shop</p>
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="/contact-us">
									<p>Contact</p>
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="/cart">
									<p>Cart</p>
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="https://twitter.com/" target="_blank" id="twitter-tooltip">
									<i className="fab fa-twitter" />
									<p className="d-lg-none d-xl-none">Twitter</p>
								</NavLink>
								<UncontrolledTooltip target="#twitter-tooltip">
									Follow us on Twitter
								</UncontrolledTooltip>
							</NavItem>
							<NavItem>
								<NavLink href="https://www.facebook.com/" target="_blank" id="facebook-tooltip">
									<i className="fab fa-facebook-square" />
									<p className="d-lg-none d-xl-none">Facebook</p>
								</NavLink>
								<UncontrolledTooltip target="#facebook-tooltip">
									Like us on Facebook
								</UncontrolledTooltip>
							</NavItem>
							<NavItem>
								<NavLink href="https://www.instagram.com/" target="_blank" id="instagram-tooltip">
									<i className="fab fa-instagram" />
									<p className="d-lg-none d-xl-none">Instagram</p>
								</NavLink>
								<UncontrolledTooltip target="#instagram-tooltip">
									Follow us on Instagram
								</UncontrolledTooltip>
							</NavItem>
							<NavItem style={{ position: 'absolute', right: '20px', bottom: '0px' }}>
								<Dropdown isOpen={dropdownOpen} toggle={toggle}>
									<DropdownToggle caret>
										<img
											src="https://lh3.googleusercontent.com/proxy/9oeC5N16n0KafM1fTH283dRTZlsUAAXIGtK95wQAnf2XT7h9LXBpIgeVNvQhRO-rMdipXPkn7R-INxLTxJIgcTC3idUKi0tilq25v6Sv11UbS77UFMmKBBkNx-M"
											className="lang-globe"
										/>
										Language
									</DropdownToggle>
									<DropdownMenu>
										<DropdownItem onClick={() => handleLanguageChange('en')}>
											<a
												href="https://es-bikespot-demo.onelink-translations.com"
												style={{ color: 'gray' }}
											>
												<img
													src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/English_language.svg/1280px-English_language.svg.png"
													className="language-image"
												/>
												English
											</a>
										</DropdownItem>
										<DropdownItem onClick={() => handleLanguageChange('es')}>
											<a
												href="https://es-bikespot-demo.onelink-translations.com"
												style={{ color: 'gray' }}
											>
												<img
													src="https://cdn.webshopapp.com/shops/94414/files/52431402/flag-of-spain.jpg"
													className="language-image"
												/>
												Spanish
											</a>
										</DropdownItem>
										<DropdownItem>
											<a style={{ color: 'gray' }} onClick={() => handleLanguageChange('pa')}>
												<img
													src="https://cdn.webshopapp.com/shops/94414/files/52410082/flag-of-pakistan.jpg"
													className="language-image"
												/>
												Punjabi
											</a>
										</DropdownItem>
									</DropdownMenu>
								</Dropdown>
							</NavItem>
						</Nav>
					</Collapse>
				</Container>
			</Navbar>
		</div>
	);
}

export default IndexNavbar;
