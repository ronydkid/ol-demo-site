/*eslint-disable*/
import React from 'react';

// reactstrap components
import { Container } from 'reactstrap';

function DarkFooter() {
	return (
		<footer className="footer" data-background-color="black">
			<Container>
				<nav>
					<ul>
						<li>
							<a href="/">Bikespot Design</a>
						</li>
						<li>
							<a href="/about">About Us</a>
						</li>
						<li>
							<a href="/" target="_blank">
								Blog
							</a>
						</li>
					</ul>
				</nav>
				<div className="copyright" id="copyright">
					© {new Date().getFullYear()} Bikespot Design
				</div>
			</Container>
		</footer>
	);
}

export default DarkFooter;
